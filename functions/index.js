// =============================================================================
console.log("============ ENV ====================");
if (process.env.NODE_ENV !== "production") {
  console.log('Not on production environment')
}

require("dotenv").config({ path: ".env" });
console.log('Current environment:', process.env.ENV);
console.log("============ ENV ====================");
// =============================================================================

// Packages
const express     = require("express");
const cors        = require("cors");
const bodyParser  = require("body-parser");
const functions   = require("firebase-functions");
const firebase    = require("firebase-admin");
const app         = express();

// Config
const firebaseConfig = {
    apiKey: "AIzaSyCFXsRTb_I4oE5venK_DDPOz5YDJgRsRV8",
    authDomain: "seedcymbiotika.firebaseapp.com",
    projectId: "seedcymbiotika",
    storageBucket: "seedcymbiotika.appspot.com",
    messagingSenderId: "1017528627790",
    appId: "1:1017528627790:web:17eb35d069f3bb36a9091f"
  };

firebase.initializeApp(firebaseConfig);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// METHODS
const RechargeCreate = require("./recharge/create");
const RechargeRemove = require("./recharge/remove");
const RechargeUpdate = require("./recharge/update");
const RechargeGet = require("./recharge/get");
const RechargeSub = require("./recharge/sub");
const RechargeMassUpdate = require("./recharge/massUpdate");

// RECHARGE MASS UPDATE SUBS TO BUNDLES
//RechargeMassUpdate.process();

// RECHARGE CHECKOUT CREATE
app.post("/api/create", async (request, res) => {
    console.log('create checkout start...')
    let response = await RechargeCreate.process(request.body)
    res.json(response);
});

// RECHARGE CHECKOUT REMOVE
app.post("/api/remove", async (request, res) => {
    console.log('remove checkout start...')
    let response = await RechargeRemove.process(request.body)
    res.json(response);
});

// RECHARGE CHECKOUT UPDATE
app.post("/api/update", async (request, res) => {
    console.log('update checkout start...')
    let response = await RechargeUpdate.process(request.body)
    res.json(response);
});

// RECHARGE CHECKOUT GET
app.post("/api/get", async (request, res) => {
    console.log('get checkout start...')
    let response = await RechargeGet.process(request.body)
    res.json(response);
});

// RECHARGE SUB BULK CREATE
app.post("/api/sub/create", async (request, res) => {
    console.log('create bulk subs start...')
    let response = await RechargeSub.create(request.body)
    res.json(response);
});

// RECHARGE SUB BULK CANCEL
app.post("/api/sub/cancel", async (request, res) => {
    console.log('cancel bulk subs start...')
    let response = await RechargeSub.cancel(request.body)
    res.json(response);
});

// RECHARGE SUB BULK DELETE
app.post("/api/sub/delete", async (request, res) => {
    console.log('delete bulk subs start...')
    let response = await RechargeSub.delete(request.body)
    res.json(response);
});

// HOME
app.get("/", async (request, res) => {
    console.log('Hello World...')
    res.send("HELLO WORLD");
});

// SERVER...
// ===========================================================================
exports.app = functions.https.onRequest(app);