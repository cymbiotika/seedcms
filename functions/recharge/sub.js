const firebase    = require('firebase-admin')
const axios       = require('axios')
let thisModule = module.exports = {
  create: async (data) => {
    let { lineItems, addressId, total, site } = data;
    let apiUrl  = `${process.env.RECHARGE_API_URL}/addresses/${addressId}/subscriptions-bulk`;
    let headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY }}
    if(site){
     headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY_TWO }}
    }
    let totalDiscount = 0;
    let discount = 0;
    let discountAmount = 1;
    
    switch(total.length) {
      case 1:
        discountAmount = .10;
        break;
      case 2:
        discountAmount = .15;
        break;
      case 3:
        discountAmount = .20;
        break;
      case 4:
        discountAmount = .25;
        break;
      case 5:
        discountAmount = .30;
        break;
      default:
        discountAmount = .30;
    }

    if(total.length > 5){
      discountAmount = .30;
    }

    for(item of lineItems){
      if(item.price > 0 && item.properties !== null && item.properties.hasOwnProperty('bundle_id')){
        discount = item.price * discountAmount;
        let price = item.price - discount;
        if(price > 20){
          item.price = price;
          totalDiscount += (discount * parseInt(item.quantity));
        }
      }
    }

    totalDiscount = totalDiscount.toFixed(2);

    let postData = {
      "subscriptions": lineItems
    };

    try{
      let res     = await axios.post(apiUrl, postData, headers)
      console.log('create bulk subscriptions complete')
      let returnObj = {
        res: res.data,
        discount: totalDiscount
      }
      return returnObj;
    } catch (e) {
        console.log('create bulk subscriptions error:')
        console.error(e.response.data);
        return 'error'
    }
  },
  delete: async (data) => {
    let { lineItems, addressId, total, site } = data;
    let apiUrl  = `${process.env.RECHARGE_API_URL}/addresses/${addressId}/subscriptions-bulk`;
    let headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY }}
    let deleteObj = {
      headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY },
      data: lineItems
    }
    if(site){
     headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY_TWO }}
     deleteObj = {
      headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY_TWO },
      data: lineItems
      }
    }

    try{     
      let res     = await  axios.delete(apiUrl, deleteObj);
      console.log('delete bulk subscriptions complete')
      let returnObj = {
        res: res.data,
        total: total
      }
      return returnObj;
    } catch (e) {
        console.log('delete bulk subscriptions error:')
        console.error(e.response.data);
        return 'error'
    }
  },
  cancel: async (data) => {
    let { lineItems, addressId, total, site } = data;
    let apiUrl  = `${process.env.RECHARGE_API_URL}/addresses/${addressId}/subscriptions-bulk`;
    let headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY }}
    if(site){
     headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY_TWO }}
    }
    try{
      let res     = await  axios.put(apiUrl, lineItems, headers);
      console.log('cancel bulk subscriptions complete')
      let returnObj = {
        res: res.data,
        total: total
      }
      return returnObj;
    } catch (e) {
        console.log('cancel bulk subscriptions error:')
        console.error(e.response.data.errors[0].errors);
        return 'error'
    }
  }
}
