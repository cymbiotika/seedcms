const firebase    = require('firebase-admin')
const axios       = require('axios')
let thisModule = module.exports = {
  process: async (data) => {
    let { lineItems, checkout, total, site } = data;
    let apiUrl  = `${process.env.RECHARGE_API_URL}/checkouts`;
    let headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY }}
    if(site){
     headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY_TWO }}
    }
    let totalDiscount = 0;
    let discount = 0;
    let discountAmount = 1;
    
    switch(total.length) {
      case 1:
        discountAmount = .10;
        break;
      case 2:
        discountAmount = .15;
        break;
      case 3:
        discountAmount = .20;
        break;
      case 4:
        discountAmount = .25;
        break;
      case 5:
        discountAmount = .30;
        break;
      default:
        discountAmount = .30;
    }

    if(total.length > 5){
      discountAmount = .30;
    }

    for(item of lineItems){
      if(item.price > 0 && item.properties !== null && item.properties.hasOwnProperty('bundle_id')){
        discount = item.price * discountAmount;
        let price = item.price - discount;
        if(price > 20){
          item.price = price;
          totalDiscount += (discount * parseInt(item.quantity));
        }
      }
    }

    totalDiscount = totalDiscount.toFixed(2);

    let postData = {
      "line_items": lineItems
    };

    try{
      let res     = await axios.post(apiUrl, postData, headers)
      console.log('create checkout complete')
      let returnObj = {
        res: res.data,
        discount: totalDiscount
      }
      return returnObj;
    } catch (e) {
        console.log('create checkout error:')
        console.error(e.response.data);
        console.error(e.response.data.errors.line_items);
        return 'error'
    }
  }
}
