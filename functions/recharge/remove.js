const firebase    = require('firebase-admin')
const axios       = require('axios')
let thisModule = module.exports = {
  process: async (data) => {
    let { } = data;
    let apiUrl  = `${process.env.RECHARGE_API_URL}/`;
    let headers = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Recharge-Access-Token': process.env.RECHARGE_API_KEY }}

    try{
      let res     = await axios.post(apiUrl, headers)
      console.log('remove checkout complete')
      return res;
    } catch (e) {
        console.error(e);
        return 'error'
    }
  }
}
